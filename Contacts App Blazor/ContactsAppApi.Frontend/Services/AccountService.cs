﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ContactsAppApi.Shared.DTO;
using Microsoft.AspNetCore.Components;

namespace ContactsAppApi.Frontend.Services
{
    public class AccountService
    {
        private readonly string apiUrl = "https://localhost:5001/api/v2/accounts";
        private readonly HttpClient httpClient;
        private readonly TokenAuthenticationStateProvider tokenAuthenticationStateProvider;

        public AccountService(
            HttpClient httpClient,
            TokenAuthenticationStateProvider tokenAuthenticationStateProvider)
        {
            this.httpClient = httpClient;
            this.tokenAuthenticationStateProvider = tokenAuthenticationStateProvider;
        }

        public async Task LoginAsync(AccountCredentialsDTO credentials)
        {
            var response = await httpClient.PostJsonAsync<AuthResponseDTO>($"{apiUrl}/login", credentials);
            await tokenAuthenticationStateProvider.SetTokenAsync(response.AccessToken, DateTime.Now.AddDays(15));
        }

        public Task RegisterAsync(AccountCredentialsDTO credentials)
        {
            return httpClient.PostJsonAsync($"{apiUrl}/register", credentials);
        }

        public Task<AccountCredentialsDTO> RefreshAsync(string refreshToken)
        {
            return httpClient.GetJsonAsync<AccountCredentialsDTO>($"{apiUrl}/refresh/{refreshToken}");
        }

        public async Task LogoutAsync()
        {
            await tokenAuthenticationStateProvider.SetTokenAsync(null);
            await httpClient.GetAsync($"{apiUrl}/logout");
        }

        public async Task<IEnumerable<UserDTO>> GetAllAccounts()
        {
            var token = await tokenAuthenticationStateProvider.GetTokenAsync();
            
            if (token != null)
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            
            return await httpClient.GetJsonAsync<IEnumerable<UserDTO>>(apiUrl);
        }
    }
}
