﻿using ContactsAppApi.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;

namespace ContactsAppApi.Frontend.Services
{
    public class ContactRepository
    {
        private readonly string apiUrl = "https://localhost:5001/api/v2/contacts";
        private readonly HttpClient httpClient;

        public ContactRepository(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public Task<IEnumerable<ContactDTO>> GetAllAsync()
        {
           return httpClient.GetJsonAsync<IEnumerable<ContactDTO>>(apiUrl);
        }

        public Task<ContactDTO> GetByIdAsync(string id)
        {
            return httpClient.GetJsonAsync<ContactDTO>($"{apiUrl}/{id}");
        }

        public Task<ContactDTO> CreateAsync(ContactDTO contact)
        {
            return httpClient.PostJsonAsync<ContactDTO>(apiUrl, contact);
        }

        public async Task DeleteAsync(string id)
        {
            await httpClient.DeleteAsync($"{apiUrl}/{id}");
        }
    }
}
