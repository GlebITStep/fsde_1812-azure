﻿window.showModal = function () {
    jQuery('#modal').modal('show');
};

window.hideModal = function () {
    jQuery('#modal').modal('hide');
};