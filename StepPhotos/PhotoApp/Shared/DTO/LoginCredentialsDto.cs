﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoApp.Shared.DTO
{
    public class LoginCredentialsDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
