﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoApp.Shared.DTO
{
    public class ProfileDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public string AppUserId { get; set; }
        public string UserName { get; set; }
    }
}
