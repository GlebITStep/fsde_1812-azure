﻿using Blazor.Extensions.Storage.Interfaces;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace StepPhotosApp.Client.Services
{
    public class TokenAuthenticationStateProvider : AuthenticationStateProvider
    {
        private readonly ILocalStorage localStorage;

        public TokenAuthenticationStateProvider(
            ILocalStorage localStorage)
        {
            this.localStorage = localStorage;
        }

        public async Task SetTokensAsync(string accessToken, string refreshToken)
        {
            await localStorage.SetItem("accessToken", accessToken);
            await localStorage.SetItem("refreshToken", refreshToken);
            NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
        }

        public async Task<string> GetAccessToken()
        {
            return await localStorage.GetItem<string>("accessToken");
        }

        public async Task<string> GetRefreshToken()
        {
            return await localStorage.GetItem<string>("refreshToken");
        }

        public async Task RemoveTokens()
        {
            await localStorage.RemoveItem("accessToken");
            await localStorage.RemoveItem("refreshToken");
            NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var token = await GetAccessToken();
            var identity = string.IsNullOrEmpty(token)
                ? new ClaimsIdentity()
                : new ClaimsIdentity(ParseClaimsFromJwt(token), "jwt");

            var name = identity.FindFirst("unique_name")?.Value;
            if (!String.IsNullOrWhiteSpace(name))
                identity.AddClaim(new Claim(ClaimsIdentity.DefaultNameClaimType, identity.FindFirst("unique_name").Value));

            return new AuthenticationState(new ClaimsPrincipal(identity));
        }

        private static IEnumerable<Claim> ParseClaimsFromJwt(string jwt)
        {
            var payload = jwt.Split('.')[1];
            var jsonBytes = ParseBase64WithoutPadding(payload);
            var keyValuePairs = JsonSerializer.Deserialize<Dictionary<string, object>>(jsonBytes);
            return keyValuePairs.Select(kvp => new Claim(kvp.Key, kvp.Value.ToString()));
        }

        private static byte[] ParseBase64WithoutPadding(string base64)
        {
            switch (base64.Length % 4)
            {
                case 2: base64 += "=="; break;
                case 3: base64 += "="; break;
            }
            return Convert.FromBase64String(base64);
        }
    }
}
