﻿using Microsoft.AspNetCore.Components;
using PhotoApp.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace StepPhotosApp.Client.Services
{
    public class AccountService
    {
        private readonly TokenAuthenticationStateProvider tokenAuthenticationStateProvider;
        private readonly HttpClient httpClient;

        public AccountService(
            TokenAuthenticationStateProvider tokenAuthenticationStateProvider,
            HttpClient httpClient)
        {
            this.tokenAuthenticationStateProvider = tokenAuthenticationStateProvider;
            this.httpClient = httpClient;
        }

        public Task<ProfileDto> GetProfile(string username)
        {
            return httpClient.GetJsonAsync<ProfileDto>($"/api/profiles/{username}");
        }

        public async Task Login(LoginCredentialsDto credentials)
        {
            var response = await httpClient.PostJsonAsync<AuthenticationResponseDto>("/api/Authentication/login", credentials);
            await tokenAuthenticationStateProvider.SetTokensAsync(response.AccessToken, response.RefreshToken);
        }

        public async Task Logout()
        {
            //var response = await httpClient.PostJsonAsync<AuthenticationResponseDto>("/api/Authentication/logout", credentials);
            await tokenAuthenticationStateProvider.RemoveTokens();
        }
    }
}
