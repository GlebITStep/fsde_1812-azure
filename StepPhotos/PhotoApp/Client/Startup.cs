using Blazor.Extensions.Storage;
using FluentValidation;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Builder;
using Microsoft.Extensions.DependencyInjection;
using PhotoApp.Shared.DTO.Validators;
using StepPhotosApp.Client.Services;

namespace StepPhotosApp.Client
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddValidatorsFromAssemblyContaining<LoginCredentialsDtoValidator>();
            services.AddStorage();

            services.AddSingleton<AccountService>();
            services.AddSingleton<TokenAuthenticationStateProvider>();
            services.AddSingleton<AuthenticationStateProvider, TokenAuthenticationStateProvider>();

            services.AddAuthorizationCore();
        }

        public void Configure(IComponentsApplicationBuilder app)
        {
            app.AddComponent<App>("app");
        }
    }
}
