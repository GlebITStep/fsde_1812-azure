﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PhotoApp.Server.Areas.Identity.Models;
using PhotoApp.Server.Data;
using PhotoApp.Server.Models;
using PhotoApp.Shared.DTO;

namespace PhotoApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfilesController : ControllerBase
    {
        private readonly UserManager<AppUser> userManager;
        private readonly ResourcesDbContext resourcesDbContext;
        private readonly IWebHostEnvironment environment;

        public ProfilesController(
            UserManager<AppUser> userManager,
            ResourcesDbContext resourcesDbContext,
            IWebHostEnvironment environment)
        {
            this.userManager = userManager;
            this.resourcesDbContext = resourcesDbContext;
            this.environment = environment;
        }

        [HttpGet]
        [Authorize]
        public async Task<IEnumerable<Profile>> GetAsync()
        {
            return await resourcesDbContext.Profiles.ToListAsync();
        }

        [HttpGet("{username}")]
        public async Task<ActionResult<Profile>> GetByUserNameAsync(string username)
        {
            var profile = await resourcesDbContext.Profiles.FirstOrDefaultAsync(x => x.UserName == username);

            if (profile == null)
                return NotFound();

            return profile;
        }

        [HttpPost]
        public async Task<ActionResult<Profile>> PostAsync(RegistrationCredentialsDto dto)
        {
            var user = new AppUser
            {
                Email = dto.Email,
                UserName = dto.Email
            };
            var result = await userManager.CreateAsync(user, dto.Password);
            if (!result.Succeeded)
                return BadRequest(result.Errors);

            var profile = new Profile
            {
                Name = dto.Name,
                PhotoUrl = "img/default.png",
                AppUserId = user.Id,
                UserName = user.UserName
            };
            resourcesDbContext.Profiles.Add(profile);
            await resourcesDbContext.SaveChangesAsync();

            return profile;
        }

        [HttpPost("upload")]
        public async Task<IActionResult> Upload()
        {
            if (HttpContext.Request.Form.Files.Any())
            {
                foreach (var file in HttpContext.Request.Form.Files)
                {
                    var blobServiceClient = new BlobServiceClient("DefaultEndpointsProtocol=https;AccountName=stepphotoappstorage;AccountKey=cM5GtKAg5BR7Cfnr5KFoDkEs/Bs5gRANLDDxaT05j/rT7imV2nIkt7pKbXBGWWhKhQVPJw6e4SiVZzMrjhofbg==;EndpointSuffix=core.windows.net");
                    var containerClient = blobServiceClient.GetBlobContainerClient("uploads");
                    await containerClient.UploadBlobAsync(file.FileName, file.OpenReadStream());

                    //var path = Path.Combine(environment.ContentRootPath, "wwwroot/uploads", file.FileName);
                    //using (var stream = new FileStream(path, FileMode.Create))
                    //{
                    //    await file.CopyToAsync(stream);
                    //}
                }
            }
            return Ok();
        }

        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
