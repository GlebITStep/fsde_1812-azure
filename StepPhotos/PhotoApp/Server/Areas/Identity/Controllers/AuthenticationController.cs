﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PhotoApp.Server.Areas.Identity.Data;
using PhotoApp.Server.Areas.Identity.Models;
using PhotoApp.Server.Areas.Identity.Services;
using PhotoApp.Shared.DTO;

namespace PhotoApp.Server.Areas.Identity.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly UserManager<AppUser> userManager;
        private readonly TokenGenerator tokenGenerator;
        private readonly AccountsDbContext accountsDbContext;

        public AuthenticationController(
            UserManager<AppUser> userManager,
            TokenGenerator tokenGenerator,
            AccountsDbContext accountsDbContext)
        {
            this.userManager = userManager;
            this.tokenGenerator = tokenGenerator;
            this.accountsDbContext = accountsDbContext;
        }

        [HttpPost("login")]
        public async Task<ActionResult<AuthenticationResponseDto>> LoginAsync(LoginCredentialsDto dto)
        {
            var user = await userManager.FindByNameAsync(dto.Email);

            if (user == null)
                return BadRequest();

            if (!await userManager.CheckPasswordAsync(user, dto.Password))
                return BadRequest();

            var accessToken = tokenGenerator.GenerateAccessToken(user);
            var refreshToken = tokenGenerator.GenerateRefreshToken();

            accountsDbContext.RefreshTokens.Add(new RefreshToken
            {
                Token = refreshToken,
                ExpiresAt = DateTime.Now.Add(tokenGenerator.Options.RefreshExpiration),
                AppUserId = user.Id
            });
            await accountsDbContext.SaveChangesAsync();

            var response = new AuthenticationResponseDto
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken
            };
            return response;
        }

        [HttpPost("refresh/{oldRefreshToken}")]
        public async Task<ActionResult<AuthenticationResponseDto>> RefreshAsync(string oldRefreshToken)
        {
            var token = await accountsDbContext.RefreshTokens.FindAsync(oldRefreshToken);

            if (token == null)
                return BadRequest();

            accountsDbContext.RefreshTokens.Remove(token);

            if (token.ExpiresAt < DateTime.Now)
                return BadRequest();

            var user = await userManager.FindByIdAsync(token.AppUserId);

            var accessToken = tokenGenerator.GenerateAccessToken(user);
            var refreshToken = tokenGenerator.GenerateRefreshToken();

            accountsDbContext.RefreshTokens.Add(new RefreshToken
            {
                Token = refreshToken,
                ExpiresAt = DateTime.Now.Add(tokenGenerator.Options.RefreshExpiration),
                AppUserId = user.Id
            });
            await accountsDbContext.SaveChangesAsync();

            var response = new AuthenticationResponseDto
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken
            };
            return response;
        }

        [HttpPost("logout/{refreshToken}")]
        public async Task<IActionResult> LogoutAsync(string refreshToken)
        {
            var token = accountsDbContext.RefreshTokens.Find(refreshToken);
            if (token != null)
            {
                accountsDbContext.RefreshTokens.Remove(token);
                await accountsDbContext.SaveChangesAsync();
            }
            return NoContent();
        }
    }
}