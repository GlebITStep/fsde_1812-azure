﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoApp.Server.Models
{
    public class Post
    {
        public string Id { get; set; }
        public string PhotoUrl { get; set; }
        public string Description { get; set; }
        public DateTime PublishedAt { get; set; }
        public string ProfileId { get; set; }

        public Profile Profile { get; set; }
    }
}
