﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoApp.Server.Models
{
    public class Profile
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public string AppUserId { get; set; }
        public string UserName { get; set; }
    }
}
