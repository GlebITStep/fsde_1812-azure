using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;
using PhotoApp.Server.Extensions;
using PhotoApp.Server.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PhotoApp.Server.Models;
using Microsoft.AspNetCore.Identity;
using PhotoApp.Server.Areas.Identity.Data;
using PhotoApp.Server.Areas.Identity.Models;
using AutoMapper;
using System.Reflection;
using FluentValidation.AspNetCore;

namespace PhotoApp.Server
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextPool<ResourcesDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("Resources"));
            });

            services.AddDbContextPool<AccountsDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("Accounts"));
            });

            services.AddIdentity<AppUser, IdentityRole>(options =>
            {
                options.SignIn.RequireConfirmedAccount = false;
            })
                .AddEntityFrameworkStores<AccountsDbContext>();

            services.AddResponseCompression(options =>
            {
                options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                    new[] { "application/octet-stream" });
            });

            services.AddMvc()
                .AddFluentValidation(options => options.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly()));

            services.AddSwagger();

            services.AddJwtAuthentication(Configuration["Secret"]);

            services.AddSignalR();

            services.AddAutoMapper(Assembly.GetExecutingAssembly());
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseResponseCompression();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBlazorDebugging();
            }

            app.UseStaticFiles();
            app.UseClientSideBlazorFiles<StepPhotosApp.Client.Startup>();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Contacts API V1");
                c.SwaggerEndpoint("/swagger/v2/swagger.json", "Contacts API V2");
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapFallbackToClientSideBlazor<StepPhotosApp.Client.Startup>("index.html");
            });
        }
    }
}
